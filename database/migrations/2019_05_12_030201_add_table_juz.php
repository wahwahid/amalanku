<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableJuz extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('juz', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dari_surat')->nullable(false);
            $table->integer('dari_ayat')->nullable(false);
            $table->integer('sampai_surat')->nullable(false);
            $table->integer('sampai_ayat')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('juz');
    }
}
