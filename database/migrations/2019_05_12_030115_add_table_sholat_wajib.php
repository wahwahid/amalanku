<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableSholatWajib extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sholat_wajib', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user')->unsigned()->nullable(false);
            $table->enum('tipe', ['S', 'D', 'A', 'M', 'I'])->nullable(false);
            $table->dateTime('waktu')->nullable(false);
            $table->boolean('jamaah')->nullable(false);
            $table->enum('tempat', ['R', 'M', 'L'])->nullable(false);
            $table->string('tempat_ket', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sholat_wajib');
    }
}
