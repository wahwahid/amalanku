<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableTadarus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tadarus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user')->unsigned()->nullable(false);
            $table->dateTime('waktu')->nullable(false);
            $table->integer('mulai_surat')->nullable(false);
            $table->integer('mulai_ayat')->nullable(false);
            $table->integer('akhir_surat')->nullable(false);
            $table->integer('akhir_ayat')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tadarus');
    }
}
