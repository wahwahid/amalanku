<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
// user
// penuh
// catatan
class AddTablePuasaRamadhan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puasa_ramadhan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user')->unsigned()->nullable(false);
            $table->dateTime('waktu')->nullable(false);
            $table->boolean('penuh')->nullable(false);
            $table->string('catatan', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puasa_ramadhan');
    }
}
