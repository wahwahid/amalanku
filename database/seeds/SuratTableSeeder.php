<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuratTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('surat')->insert([
            [ "id" => 1, "nama" => "Al-Fatihah", "panjang" => 7 ],
            [ "id" => 2, "nama" => "Al Baqarah", "panjang" => 286 ],
            [ "id" => 3, "nama" => "Ali 'Imran", "panjang" => 200 ],
            [ "id" => 4, "nama" => "An-Nisa'", "panjang" => 176 ],
            [ "id" => 5, "nama" => "Al - Ma'idah", "panjang" => 120 ],
            [ "id" => 6, "nama" => "Al - An'am", "panjang" => 165 ],
            [ "id" => 7, "nama" => "Al - A'raf", "panjang" => 206 ],
            [ "id" => 8, "nama" => "Al - Anfal", "panjang" => 75 ],
            [ "id" => 9, "nama" => "At - Taubah", "panjang" => 129 ],
            [ "id" => 10, "nama" => "Yunus", "panjang" => 109 ],
            [ "id" => 11, "nama" => "Hud", "panjang" => 123 ],
            [ "id" => 12, "nama" => "Yusuf", "panjang" => 111 ],
            [ "id" => 13, "nama" => "Ar- Ra'd", "panjang" => 43 ],
            [ "id" => 14, "nama" => "Ibrahim", "panjang" => 52 ],
            [ "id" => 15, "nama" => "Al - Hijr", "panjang" => 99 ],
            [ "id" => 16, "nama" => "An - Nahl", "panjang" => 128 ],
            [ "id" => 17, "nama" => "Al - Isra'", "panjang" => 111 ],
            [ "id" => 18, "nama" => "Al - Kahf", "panjang" => 110 ],
            [ "id" => 19, "nama" => "Maryam", "panjang" => 98 ],
            [ "id" => 20, "nama" => "Ta-Ha", "panjang" => 135 ],
            [ "id" => 21, "nama" => "Al - Anbiya", "panjang" => 112 ],
            [ "id" => 22, "nama" => "Al - Hajj", "panjang" => 78 ],
            [ "id" => 23, "nama" => "Al - Mu'minun", "panjang" => 118 ],
            [ "id" => 24, "nama" => "An - Nur", "panjang" => 64 ],
            [ "id" => 25, "nama" => "Al - Furqan", "panjang" => 77 ],
            [ "id" => 26, "nama" => "Asy-Syu'ara'", "panjang" => 227 ],
            [ "id" => 27, "nama" => "An-Naml", "panjang" => 93 ],
            [ "id" => 28, "nama" => "Al- Qasas", "panjang" => 88 ],
            [ "id" => 29, "nama" => "Al - 'Ankabut", "panjang" => 69 ],
            [ "id" => 30, "nama" => "Ar-Rum", "panjang" => 60 ],
            [ "id" => 31, "nama" => "Luqman", "panjang" => 34 ],
            [ "id" => 32, "nama" => "As-Sajdah", "panjang" => 30 ],
            [ "id" => 33, "nama" => "Al-Ahzab", "panjang" => 73 ],
            [ "id" => 34, "nama" => "Saba'", "panjang" => 54 ],
            [ "id" => 35, "nama" => "Fatir", "panjang" => 45 ],
            [ "id" => 36, "nama" => "Ya-Sin", "panjang" => 83 ],
            [ "id" => 37, "nama" => "As-Saffat", "panjang" => 182 ],
            [ "id" => 38, "nama" => "Sad", "panjang" => 88 ],
            [ "id" => 39, "nama" => "Az-Zumar", "panjang" => 75 ],
            [ "id" => 40, "nama" => "Ghafir", "panjang" => 85 ],
            [ "id" => 41, "nama" => "Fussilat", "panjang" => 54 ],
            [ "id" => 42, "nama" => "Asy-Syura", "panjang" => 53 ],
            [ "id" => 43, "nama" => "Az-Zukhruf", "panjang" => 89 ],
            [ "id" => 44, "nama" => "Ad-Dukhan", "panjang" => 59 ],
            [ "id" => 45, "nama" => "Al-Jasiyah", "panjang" => 37 ],
            [ "id" => 46, "nama" => "Al-Ahqaf", "panjang" => 35 ],
            [ "id" => 47, "nama" => "Muhammad", "panjang" => 38 ],
            [ "id" => 48, "nama" => "Al-Fath", "panjang" => 29 ],
            [ "id" => 49, "nama" => "Al-Hujurat", "panjang" => 18 ],
            [ "id" => 50, "nama" => "Qaf", "panjang" => 45 ],
            [ "id" => 51, "nama" => "Az-Zariyat", "panjang" => 60 ],
            [ "id" => 52, "nama" => "At-Tur", "panjang" => 49 ],
            [ "id" => 53, "nama" => "An-Najm", "panjang" => 62 ],
            [ "id" => 54, "nama" => "Al - Qamar", "panjang" => 55 ],
            [ "id" => 55, "nama" => "Ar-Rahman", "panjang" => 78 ],
            [ "id" => 56, "nama" => "Al-Waqi'ah", "panjang" => 96 ],
            [ "id" => 57, "nama" => "Al-Hadid", "panjang" => 29 ],
            [ "id" => 58, "nama" => "Al-Mujadilah", "panjang" => 22 ],
            [ "id" => 59, "nama" => "Al-Hasyr", "panjang" => 24 ],
            [ "id" => 60, "nama" => "Al-Mumtahanah", "panjang" => 13 ],
            [ "id" => 61, "nama" => "As-Saff", "panjang" => 14 ],
            [ "id" => 62, "nama" => "Al-Jum'ah", "panjang" => 11 ],
            [ "id" => 63, "nama" => "Al-Munafiqun", "panjang" => 11 ],
            [ "id" => 64, "nama" => "At-Taghabun", "panjang" => 18 ],
            [ "id" => 65, "nama" => "At-Talaq", "panjang" => 12 ],
            [ "id" => 66, "nama" => "At-Tahrim", "panjang" => 12 ],
            [ "id" => 67, "nama" => "Al-Mulk", "panjang" => 30 ],
            [ "id" => 68, "nama" => "Al-Qalam", "panjang" => 52 ],
            [ "id" => 69, "nama" => "Al-Haqqah", "panjang" => 52 ],
            [ "id" => 70, "nama" => "Al-Ma'arij", "panjang" => 44 ],
            [ "id" => 71, "nama" => "Nuh", "panjang" => 28 ],
            [ "id" => 72, "nama" => "Al-Jinn", "panjang" => 28 ],
            [ "id" => 73, "nama" => "Al-Muzzammil", "panjang" => 20 ],
            [ "id" => 74, "nama" => "Al-Muddassir", "panjang" => 56 ],
            [ "id" => 75, "nama" => "Al-Qiyamah", "panjang" => 40 ],
            [ "id" => 76, "nama" => "Al - Insan", "panjang" => 31 ],
            [ "id" => 77, "nama" => "Al-Mursalat", "panjang" => 50 ],
            [ "id" => 78, "nama" => "An-Naba'", "panjang" => 40 ],
            [ "id" => 79, "nama" => "An-Nazi'at", "panjang" => 46 ],
            [ "id" => 80, "nama" => "Abasa", "panjang" => 42 ],
            [ "id" => 81, "nama" => "At-Takwir", "panjang" => 29 ],
            [ "id" => 82, "nama" => "Al-Infitar", "panjang" => 19 ],
            [ "id" => 83, "nama" => "Al-Muthaffiin", "panjang" => 36 ],
            [ "id" => 84, "nama" => "Al-Insyiqaq", "panjang" => 25 ],
            [ "id" => 85, "nama" => "Al-Buruj", "panjang" => 22 ],
            [ "id" => 86, "nama" => "At-Tariq", "panjang" => 17 ],
            [ "id" => 87, "nama" => "Al-A'la", "panjang" => 19 ],
            [ "id" => 88, "nama" => "Al-Ghasyiyah", "panjang" => 26 ],
            [ "id" => 89, "nama" => "Al-Fajr", "panjang" => 30 ],
            [ "id" => 90, "nama" => "Al-Balad", "panjang" => 20 ],
            [ "id" => 91, "nama" => "Asy-Syams", "panjang" => 15 ],
            [ "id" => 92, "nama" => "Al-Lail", "panjang" => 21 ],
            [ "id" => 93, "nama" => "Ad-Duha", "panjang" => 11 ],
            [ "id" => 94, "nama" => "Al-Insyirah", "panjang" => 8 ],
            [ "id" => 95, "nama" => "At-Tin", "panjang" => 8 ],
            [ "id" => 96, "nama" => "Al-'Alaq", "panjang" => 19 ],
            [ "id" => 97, "nama" => "Al-Qadr", "panjang" => 5 ],
            [ "id" => 98, "nama" => "Al-Bayyinah", "panjang" => 8 ],
            [ "id" => 99, "nama" => "Az-Zalzalah", "panjang" => 8 ],
            [ "id" => 100, "nama" => "Al-'Adiyat", "panjang" => 11 ],
            [ "id" => 101, "nama" => "Al-Qari'ah", "panjang" => 11 ],
            [ "id" => 102, "nama" => "At-Takasur", "panjang" => 8 ],
            [ "id" => 103, "nama" => "Al-'Asr", "panjang" => 3 ],
            [ "id" => 104, "nama" => "Al-Humazah", "panjang" => 9 ],
            [ "id" => 105, "nama" => "Al-Fil", "panjang" => 5 ],
            [ "id" => 106, "nama" => "Quraisy", "panjang" => 4 ],
            [ "id" => 107, "nama" => "Al-Ma'un", "panjang" => 7 ],
            [ "id" => 108, "nama" => "Al-Kausar", "panjang" => 3 ],
            [ "id" => 109, "nama" => "Al-Kafirun", "panjang" => 6 ],
            [ "id" => 110, "nama" => "An-Nasr", "panjang" => 3 ],
            [ "id" => 111, "nama" => "Al-Lahab", "panjang" => 5 ],
            [ "id" => 112, "nama" => "Al-Ikhlas", "panjang" => 4 ],
            [ "id" => 113, "nama" => "Al-Falaq", "panjang" => 5 ],
            [ "id" => 114, "nama" => "An-Nas", "panjang" => 6 ]
        ]);
    }
}
