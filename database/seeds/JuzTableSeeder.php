<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JuzTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('juz')->insert([
            [ "id" => 1, "dari_surat" => 1, "dari_ayat" => 1, "sampai_surat" => 2, "sampai_ayat" => 141 ],
            [ "id" => 2, "dari_surat" => 2, "dari_ayat" => 142, "sampai_surat" => 2, "sampai_ayat" => 252 ],
            [ "id" => 3, "dari_surat" => 2, "dari_ayat" => 253, "sampai_surat" => 3, "sampai_ayat" => 91 ],
            [ "id" => 4, "dari_surat" => 4, "dari_ayat" => 92, "sampai_surat" => 4, "sampai_ayat" => 23 ],
            [ "id" => 5, "dari_surat" => 4, "dari_ayat" => 24, "sampai_surat" => 4, "sampai_ayat" => 147 ],
            [ "id" => 6, "dari_surat" => 4, "dari_ayat" => 148, "sampai_surat" => 5, "sampai_ayat" => 82 ],
            [ "id" => 7, "dari_surat" => 5, "dari_ayat" => 83, "sampai_surat" => 6, "sampai_ayat" => 110 ],
            [ "id" => 8, "dari_surat" => 6, "dari_ayat" => 111, "sampai_surat" => 7, "sampai_ayat" => 87 ],
            [ "id" => 9, "dari_surat" => 7, "dari_ayat" => 88, "sampai_surat" => 8, "sampai_ayat" => 40 ],
            [ "id" => 10, "dari_surat" => 8, "dari_ayat" => 41, "sampai_surat" => 9, "sampai_ayat" => 93 ],
            [ "id" => 11, "dari_surat" => 9, "dari_ayat" => 94, "sampai_surat" => 11, "sampai_ayat" => 5 ],
            [ "id" => 12, "dari_surat" => 11, "dari_ayat" => 6, "sampai_surat" => 12, "sampai_ayat" => 52 ],
            [ "id" => 13, "dari_surat" => 12, "dari_ayat" => 53, "sampai_surat" => 15, "sampai_ayat" => 1 ],
            [ "id" => 14, "dari_surat" => 15, "dari_ayat" => 2, "sampai_surat" => 16, "sampai_ayat" => 126 ],
            [ "id" => 15, "dari_surat" => 17, "dari_ayat" => 1, "sampai_surat" => 18, "sampai_ayat" => 74 ],
            [ "id" => 16, "dari_surat" => 18, "dari_ayat" => 75, "sampai_surat" => 20, "sampai_ayat" => 135 ],
            [ "id" => 17, "dari_surat" => 21, "dari_ayat" => 1, "sampai_surat" => 22, "sampai_ayat" => 78 ],
            [ "id" => 18, "dari_surat" => 23, "dari_ayat" => 1, "sampai_surat" => 25, "sampai_ayat" => 20 ],
            [ "id" => 19, "dari_surat" => 25, "dari_ayat" => 21, "sampai_surat" => 27, "sampai_ayat" => 59 ],
            [ "id" => 20, "dari_surat" => 27, "dari_ayat" => 60, "sampai_surat" => 29, "sampai_ayat" => 44 ],
            [ "id" => 21, "dari_surat" => 29, "dari_ayat" => 45, "sampai_surat" => 33, "sampai_ayat" => 30 ],
            [ "id" => 22, "dari_surat" => 33, "dari_ayat" => 31, "sampai_surat" => 36, "sampai_ayat" => 21 ],
            [ "id" => 23, "dari_surat" => 36, "dari_ayat" => 22, "sampai_surat" => 39, "sampai_ayat" => 31 ],
            [ "id" => 24, "dari_surat" => 39, "dari_ayat" => 32, "sampai_surat" => 41, "sampai_ayat" => 46 ],
            [ "id" => 25, "dari_surat" => 45, "dari_ayat" => 47, "sampai_surat" => 45, "sampai_ayat" => 37 ],
            [ "id" => 26, "dari_surat" => 46, "dari_ayat" => 1, "sampai_surat" => 51, "sampai_ayat" => 30 ],
            [ "id" => 27, "dari_surat" => 51, "dari_ayat" => 31, "sampai_surat" => 57, "sampai_ayat" => 29 ],
            [ "id" => 28, "dari_surat" => 58, "dari_ayat" => 1, "sampai_surat" => 66, "sampai_ayat" => 12 ],
            [ "id" => 29, "dari_surat" => 67, "dari_ayat" => 1, "sampai_surat" => 77, "sampai_ayat" => 50 ],
            [ "id" => 30, "dari_surat" => 78, "dari_ayat" => 1, "sampai_surat" => 114, "sampai_ayat" => 6 ]
        ]);
    }
}
