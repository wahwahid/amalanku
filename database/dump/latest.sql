/*
SQLyog Ultimate v11.2 (64 bit)
MySQL - 10.3.12-MariaDB : Database - akakom_amalanku
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `juz` */

DROP TABLE IF EXISTS `juz`;

CREATE TABLE `juz` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dari_surat` int(11) NOT NULL,
  `dari_ayat` int(11) NOT NULL,
  `sampai_surat` int(11) NOT NULL,
  `sampai_ayat` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `juz` */

insert  into `juz`(`id`,`dari_surat`,`dari_ayat`,`sampai_surat`,`sampai_ayat`,`created_at`,`updated_at`) values (1,1,1,2,141,NULL,NULL),(2,2,142,2,252,NULL,NULL),(3,2,253,2,286,NULL,NULL),(4,4,92,4,23,NULL,NULL),(5,4,24,4,147,NULL,NULL),(6,4,148,5,82,NULL,NULL),(7,5,83,6,110,NULL,NULL),(8,6,111,7,87,NULL,NULL),(9,7,88,8,40,NULL,NULL),(10,8,41,9,93,NULL,NULL),(11,9,94,11,5,NULL,NULL),(12,11,6,12,52,NULL,NULL),(13,12,53,15,1,NULL,NULL),(14,15,2,16,126,NULL,NULL),(15,17,1,18,74,NULL,NULL),(16,18,75,20,135,NULL,NULL),(17,21,1,22,78,NULL,NULL),(18,23,1,25,20,NULL,NULL),(19,25,21,27,59,NULL,NULL),(20,27,60,29,44,NULL,NULL),(21,29,45,33,30,NULL,NULL),(22,33,31,36,21,NULL,NULL),(23,36,22,39,31,NULL,NULL),(24,39,32,41,46,NULL,NULL),(25,45,47,45,37,NULL,NULL),(26,46,1,51,30,NULL,NULL),(27,51,31,57,29,NULL,NULL),(28,58,1,66,12,NULL,NULL),(29,67,1,77,50,NULL,NULL),(30,78,1,114,6,NULL,NULL);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2019_05_12_025439_user_add_gender_birthday',1),(3,'2019_05_12_030115_add_table_sholat_wajib',1),(4,'2019_05_12_030153_add_table_surat',1),(5,'2019_05_12_030201_add_table_juz',1),(6,'2019_05_12_030215_add_table_tadarus',1),(7,'2019_05_12_030256_add_table_puasa_ramadhan',1);

/*Table structure for table `puasa_ramadhan` */

DROP TABLE IF EXISTS `puasa_ramadhan`;

CREATE TABLE `puasa_ramadhan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL,
  `waktu` datetime NOT NULL,
  `penuh` tinyint(1) NOT NULL,
  `catatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `puasa_ramadhan` */

insert  into `puasa_ramadhan`(`id`,`user`,`waktu`,`penuh`,`catatan`,`created_at`,`updated_at`) values (1,2,'2019-05-10 05:25:15',1,NULL,NULL,NULL),(2,2,'2019-05-11 05:26:14',1,NULL,NULL,NULL);

/*Table structure for table `sholat_wajib` */

DROP TABLE IF EXISTS `sholat_wajib`;

CREATE TABLE `sholat_wajib` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL,
  `tipe` enum('S','D','A','M','I') COLLATE utf8mb4_unicode_ci NOT NULL,
  `waktu` datetime NOT NULL,
  `jamaah` tinyint(1) NOT NULL,
  `tempat` enum('R','M','L') COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_ket` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sholat_wajib` */

insert  into `sholat_wajib`(`id`,`user`,`tipe`,`waktu`,`jamaah`,`tempat`,`tempat_ket`,`created_at`,`updated_at`) values (1,8,'S','2019-05-17 05:26:26',1,'M','Al muhtadin',NULL,NULL),(2,8,'D','2019-05-17 12:26:26',1,'L','Kantor',NULL,NULL),(3,8,'A','2019-05-17 15:26:26',0,'L','Kantor',NULL,NULL),(4,8,'M','2019-05-17 18:27:09',1,'L','Kampus',NULL,NULL),(5,8,'I','2019-05-17 19:27:09',1,'L','Kampus',NULL,NULL),(6,8,'S','2019-05-18 05:26:26',0,'R',NULL,NULL,NULL),(7,8,'D','2019-05-18 13:10:30',0,'M','Al Hasanah','2019-05-18 18:41:42','2019-05-18 18:41:42'),(8,6,'M','2019-06-27 17:40:22',1,'M','Akakom','2019-06-27 16:10:42','2019-06-27 16:10:42');

/*Table structure for table `surat` */

DROP TABLE IF EXISTS `surat`;

CREATE TABLE `surat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `panjang` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `surat` */

insert  into `surat`(`id`,`nama`,`panjang`,`created_at`,`updated_at`) values (1,'Al Fatihah',7,NULL,NULL),(2,'Al Baqarah',286,NULL,NULL),(3,'Ali \'Imran',200,NULL,NULL),(4,'An-Nisa\'',176,NULL,NULL),(5,'Al - Ma\'idah',120,NULL,NULL),(6,'Al - An\'am',165,NULL,NULL),(7,'Al - A\'raf',206,NULL,NULL),(8,'Al - Anfal',75,NULL,NULL),(9,'At - Taubah',129,NULL,NULL),(10,'Yunus',109,NULL,NULL),(11,'Hud',123,NULL,NULL),(12,'Yusuf',111,NULL,NULL),(13,'Ar- Ra\'d',43,NULL,NULL),(14,'Ibrahim',52,NULL,NULL),(15,'Al - Hijr',99,NULL,NULL),(16,'An - Nahl',128,NULL,NULL),(17,'Al - Isra\'',111,NULL,NULL),(18,'Al - Kahf',110,NULL,NULL),(19,'Maryam',98,NULL,NULL),(20,'Ta-Ha',135,NULL,NULL),(21,'Al - Anbiya',112,NULL,NULL),(22,'Al - Hajj',78,NULL,NULL),(23,'Al - Mu\'minun',118,NULL,NULL),(24,'An - Nur',64,NULL,NULL),(25,'Al - Furqan',77,NULL,NULL),(26,'Asy-Syu\'ara\'',227,NULL,NULL),(27,'An-Naml',93,NULL,NULL),(28,'Al- Qasas',88,NULL,NULL),(29,'Al - \'Ankabut',69,NULL,NULL),(30,'Ar-Rum',60,NULL,NULL),(31,'Luqman',34,NULL,NULL),(32,'As-Sajdah',30,NULL,NULL),(33,'Al-Ahzab',73,NULL,NULL),(34,'Saba\'',54,NULL,NULL),(35,'Fatir',45,NULL,NULL),(36,'Ya-Sin',83,NULL,NULL),(37,'As-Saffat',182,NULL,NULL),(38,'Sad',88,NULL,NULL),(39,'Az-Zumar',75,NULL,NULL),(40,'Ghafir',85,NULL,NULL),(41,'Fussilat',54,NULL,NULL),(42,'Asy-Syura',53,NULL,NULL),(43,'Az-Zukhruf',89,NULL,NULL),(44,'Ad-Dukhan',59,NULL,NULL),(45,'Al-Jasiyah',37,NULL,NULL),(46,'Al-Ahqaf',35,NULL,NULL),(47,'Muhammad',38,NULL,NULL),(48,'Al-Fath',29,NULL,NULL),(49,'Al-Hujurat',18,NULL,NULL),(50,'Qaf',45,NULL,NULL),(51,'Az-Zariyat',60,NULL,NULL),(52,'At-Tur',49,NULL,NULL),(53,'An-Najm',62,NULL,NULL),(54,'Al - Qamar',55,NULL,NULL),(55,'Ar-Rahman',78,NULL,NULL),(56,'Al-Waqi\'ah',96,NULL,NULL),(57,'Al-Hadid',29,NULL,NULL),(58,'Al-Mujadilah',22,NULL,NULL),(59,'Al-Hasyr',24,NULL,NULL),(60,'Al-Mumtahanah',13,NULL,NULL),(61,'As-Saff',14,NULL,NULL),(62,'Al-Jum\'ah',11,NULL,NULL),(63,'Al-Munafiqun',11,NULL,NULL),(64,'At-Taghabun',18,NULL,NULL),(65,'At-Talaq',12,NULL,NULL),(66,'At-Tahrim',12,NULL,NULL),(67,'Al-Mulk',30,NULL,NULL),(68,'Al-Qalam',52,NULL,NULL),(69,'Al-Haqqah',52,NULL,NULL),(70,'Al-Ma\'arij',44,NULL,NULL),(71,'Nuh',28,NULL,NULL),(72,'Al-Jinn',28,NULL,NULL),(73,'Al-Muzzammil',20,NULL,NULL),(74,'Al-Muddassir',56,NULL,NULL),(75,'Al-Qiyamah',40,NULL,NULL),(76,'Al - Insan',31,NULL,NULL),(77,'Al-Mursalat',50,NULL,NULL),(78,'An-Naba\'',40,NULL,NULL),(79,'An-Nazi\'at',46,NULL,NULL),(80,'Abasa',42,NULL,NULL),(81,'At-Takwir',29,NULL,NULL),(82,'Al-Infitar',19,NULL,NULL),(83,'Al-Muthaffiin',36,NULL,NULL),(84,'Al-Insyiqaq',25,NULL,NULL),(85,'Al-Buruj',22,NULL,NULL),(86,'At-Tariq',17,NULL,NULL),(87,'Al-A\'la',19,NULL,NULL),(88,'Al-Ghasyiyah',26,NULL,NULL),(89,'Al-Fajr',30,NULL,NULL),(90,'Al-Balad',20,NULL,NULL),(91,'Asy-Syams',15,NULL,NULL),(92,'Al-Lail',21,NULL,NULL),(93,'Ad-Duha',11,NULL,NULL),(94,'Al-Insyirah',8,NULL,NULL),(95,'At-Tin',8,NULL,NULL),(96,'Al-\'Alaq',19,NULL,NULL),(97,'Al-Qadr',5,NULL,NULL),(98,'Al-Bayyinah',8,NULL,NULL),(99,'Az-Zalzalah',8,NULL,NULL),(100,'Al-\'Adiyat',11,NULL,NULL),(101,'Al-Qari\'ah',11,NULL,NULL),(102,'At-Takasur',8,NULL,NULL),(103,'Al-\'Asr',3,NULL,NULL),(104,'Al-Humazah',9,NULL,NULL),(105,'Al-Fil',5,NULL,NULL),(106,'Quraisy',4,NULL,NULL),(107,'Al-Ma\'un',7,NULL,NULL),(108,'Al-Kausar',3,NULL,NULL),(109,'Al-Kafirun',6,NULL,NULL),(110,'An-Nasr',3,NULL,NULL),(111,'Al-Lahab',5,NULL,NULL),(112,'Al-Ikhlas',4,NULL,NULL),(113,'Al-Falaq',5,NULL,NULL),(114,'An-Nas',6,NULL,NULL);

/*Table structure for table `tadarus` */

DROP TABLE IF EXISTS `tadarus`;

CREATE TABLE `tadarus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL,
  `waktu` datetime NOT NULL,
  `mulai_surat` int(11) NOT NULL,
  `mulai_ayat` int(11) NOT NULL,
  `akhir_surat` int(11) NOT NULL,
  `akhir_ayat` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `tadarus` */

insert  into `tadarus`(`id`,`user`,`waktu`,`mulai_surat`,`mulai_ayat`,`akhir_surat`,`akhir_ayat`,`created_at`,`updated_at`) values (1,8,'2019-05-11 05:28:08',1,1,2,10,NULL,NULL),(2,8,'2019-05-11 18:28:20',2,11,2,30,NULL,NULL),(3,8,'2019-05-12 05:28:41',2,31,2,130,NULL,NULL),(4,8,'2019-05-12 17:39:46',2,131,2,138,'2019-05-12 16:12:29','2019-05-12 16:12:29'),(5,8,'2019-05-12 17:43:35',2,139,2,144,'2019-05-12 16:13:43','2019-05-12 16:13:43'),(6,8,'2019-05-12 17:44:27',2,145,2,148,'2019-05-12 16:14:37','2019-05-12 16:14:37'),(7,8,'2019-05-12 17:45:02',2,149,2,151,'2019-05-12 16:15:12','2019-05-12 16:15:12'),(8,8,'2019-05-12 17:45:31',2,152,2,156,'2019-05-12 16:15:40','2019-05-12 16:15:40'),(9,8,'2019-05-12 17:47:43',2,157,2,162,'2019-05-12 16:18:02','2019-05-12 16:18:02'),(10,8,'2019-05-12 17:48:19',2,163,2,167,'2019-05-12 16:18:27','2019-05-12 16:18:27'),(11,2,'2019-07-01 00:00:00',1,1,2,2,'2019-07-09 21:00:00','2019-07-09 21:00:00');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`remember_token`,`gender`,`birthday`,`created_at`,`updated_at`) values (1,'Administrator','admin@gmail.com','2019-05-09 04:24:10','$2y$10$mbheFoVNQxwILH8wbjY64.dBnFhJdjkBemzV8VfyjxFBg0Mkn5xFq','wCEdnAyhVR',NULL,NULL,'2019-05-09 04:24:10','2019-05-09 04:24:10'),(2,'Wahid','dev@wahwahid.com',NULL,'$2y$10$HP0vHpwdatPm2S8yWIhMgeUGsK1867iJ9NHhv3aLSDkDv3ITfF.Q2',NULL,1,'1996-12-29 00:00:00','2019-05-09 04:30:56','2019-05-12 06:35:54'),(6,'Khanifa','khanifaanip@gmail.com',NULL,'$2y$10$1OZbs576YnMCV4K/T57P0.2MeVsVX3SI4AVknLDsxl8hKZxva4DDe',NULL,0,'1997-06-28 00:00:00','2019-06-09 16:07:50','2019-06-27 16:07:50'),(7,'Syaiful','sfmusta@gmail.com',NULL,'$2y$10$zMNywgPCkDW.Z8oX8C.sxuPeFSt2T30x73vaOmjq96EXOfRaZbxt6',NULL,1,'1993-06-24 00:00:00','2019-06-09 19:54:53','2019-06-27 19:54:53'),(8,'Anonim','user@email.com',NULL,'$2y$10$FY7kdpvA.ME/TvgOeMk4v.DpXoGBgmFHOzGqj0U6wNcesIspB7aMe',NULL,1,'1995-07-24 00:00:00','2019-06-09 20:57:07','2019-06-29 20:57:07');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
