import { HTTP } from './http'

const SholatJadwal = {
    daily (city) {
        return HTTP.get(`sholat-jadwal/${city}`)
    }
}

export { SholatJadwal }
