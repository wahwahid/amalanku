import Axios from 'axios'

const Location = {
    geo () {
        return Axios.get(`https://ipinfo.io/geo?token=877a9deaf931f3`)
    },
    city () {
        return Axios.get(`https://ipinfo.io/city?token=877a9deaf931f3`)
    },
    region () {
        return Axios.get(`https://ipinfo.io/region?token=877a9deaf931f3`)
    }
}

export { Location }
