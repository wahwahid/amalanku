import { HTTP } from './http'

const PuasaRamadhan = {
    list () {
        return HTTP.get('puasa-ramadhan')
    },

    show (id) {
        return HTTP.get(`puasa-ramadhan/${id}`)
    },

    store (data) {
        return HTTP.post('puasa-ramadhan', data)
    }
}

export { PuasaRamadhan }
