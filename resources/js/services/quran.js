import { HTTP } from './http'

const Quran = {
    list () {
        return HTTP.get('quran')
    }
}

export { Quran }
