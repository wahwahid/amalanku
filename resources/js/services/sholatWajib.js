import { HTTP } from './http'

const SholatWajib = {
    list (year, month) {
        return HTTP.get('sholat-wajib', {
            params: {
                year,
                month
            }
        })
    },

    show (id) {
        return HTTP.get(`sholat-wajib/${id}`)
    },

    store (data) {
        return HTTP.post('sholat-wajib', data)
    }
}

export { SholatWajib }
