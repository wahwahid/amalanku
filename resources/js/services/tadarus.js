import { HTTP } from './http'

const Tadarus = {
    list () {
        return HTTP.get('tadarus')
    },

    show (id) {
        return HTTP.get(`tadarus/${id}`)
    },

    store (data) {
        return HTTP.post('tadarus', data)
    }
}

export { Tadarus }
