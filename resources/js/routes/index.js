import VueRouter from 'vue-router'
import Vue from 'vue'
import Home from '../components/Home'
import About from '../components/About'
import Login from '../components/Auth/Login'
import NotFound from '../components/404'
import Register from '../components/Auth/Register'
import Profile from '../components/User/Profile'
import SholatWajib from '../components/SholatWajib'
import Tadarus from '../components/Tadarus'
import { AuthGuard } from '../middleware/auth'
import { GuestGuard } from '../middleware/guest'

Vue.use(VueRouter)

const routes = new VueRouter({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home,
            meta: {
                title: 'Home'
            }
        }, {
            path: '/about',
            name: 'About',
            component: About,
            meta: {
                title: 'About'
            }
        }, {
            path: '/profile',
            name: 'Profile',
            component: Profile,
            beforeEnter: AuthGuard,
            meta: {
                title: 'User Profile'
            }
        }, {
            path: '/login',
            name: 'Login',
            component: Login,
            beforeEnter: GuestGuard,
            meta: {
                title: 'Login'
            }
        }, {
            path: '/register',
            name: 'Register',
            component: Register,
            beforeEnter: GuestGuard,
            meta: {
                title: 'Register'
            }
        }, {
            path: '/sholat',
            name: 'SholatWajib',
            component: SholatWajib,
            beforeEnter: AuthGuard,
            meta: {
                title: 'Sholat Wajib'
            }
        }, {
            path: '/tadarus',
            name: 'Tadarus',
            component: Tadarus,
            beforeEnter: AuthGuard,
            meta: {
                title: 'Tadarus'
            }
        }, {
            path: '*',
            name: 'NotFound',
            component: NotFound
        }
    ],
    mode: 'history'
})

routes.beforeEach((to, from, next) => {
    document.title = to.meta.title ? to.meta.title : 'Untitled'
    next()
})

export default routes
