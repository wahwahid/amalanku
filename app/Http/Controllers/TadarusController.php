<?php

namespace App\Http\Controllers;

use App\Tadarus;
use Illuminate\Http\Request;

class TadarusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Tadarus::with('user:id')->where('user', '=', auth()->user()->id)->get();
        return response()->send('Tadarus', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'waktu' => 'required',
            'mulai_surat' => 'required',
            'mulai_ayat' => 'required',
            'akhir_surat' => 'required',
            'akhir_ayat' => 'required'
        ]);
        Tadarus::create($request->all());
        return response()->send('Tadarus Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tadarus  $tadarus
     * @return \Illuminate\Http\Response
     */
    public function show(Tadarus $tadarus)
    {
        return response()->send('Data', $tadarus);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tadarus  $tadarus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tadarus $tadarus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tadarus  $tadarus
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tadarus $tadarus)
    {
        //
    }
}
