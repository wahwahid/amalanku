<?php

namespace App\Http\Controllers;

use App\SholatWajib;
use Illuminate\Http\Request;

class SholatWajibController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $year = $request->query('year', date('Y'));
        $month = $request->query('month', date('n'));
        $data = SholatWajib::with('user:id')->whereYear('waktu', '=', $year)->whereMonth('waktu', '=', $month)->where('user', '=', auth()->user()->id)->get();
        return response()->send('SholatWajib', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tipe' => 'required',
            'waktu' => 'required',
            'jamaah' => 'required',
            'tempat' => 'required'
        ]);
        SholatWajib::create($request->all());
        return response()->send('SholatWajib Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SholatWajib  $sholatWajib
     * @return \Illuminate\Http\Response
     */
    public function show(SholatWajib $sholatWajib)
    {
        return response()->send('Data', $sholatWajib);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SholatWajib  $sholatWajib
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SholatWajib $sholatWajib)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SholatWajib  $sholatWajib
     * @return \Illuminate\Http\Response
     */
    public function destroy(SholatWajib $sholatWajib)
    {
        //
    }
}
