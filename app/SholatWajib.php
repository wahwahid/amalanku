<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SholatWajib extends Model
{
    protected $table = 'sholat_wajib';

    protected $fillable = [
        'waktu',
        'tipe',
        'jamaah',
        'tempat',
        'tempat_ket'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user');
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($sholat_wajib) {
            $sholat_wajib->user = auth()->user()->id;
        });
    }
}
