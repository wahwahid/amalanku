<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Juz extends Model
{
    protected $table = 'juz';
}
