<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tadarus extends Model
{
    protected $table = 'tadarus';

    protected $fillable = [
        'waktu',
        'mulai_surat',
        'mulai_ayat',
        'akhir_surat',
        'akhir_ayat'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user');
    }

    public function mulai()
    {
        return $this->belongsTo('App\Surat', 'mulai_surat');
    }

    public function akhir()
    {
        return $this->belongsTo('App\Surat', 'akhir_surat');
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($tadarus) {
            $tadarus->user = auth()->user()->id;
        });
    }
}
